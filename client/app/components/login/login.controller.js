class LoginController {
  constructor($rootScope, $state, ConfirmationModalService) {
    'ngInject';
    this.name = $rootScope.username;
    this.login = this.login.bind(this, $rootScope, $state, ConfirmationModalService);
  }

  login($rootScope, $state, ConfirmationModalService) {
    if (this.name) {
      $rootScope.username = this.name;
    } else {
      ConfirmationModalService.show("Please select a username.", () => { });
    }
  }
}

export default LoginController;
