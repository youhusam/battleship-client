class ChatController {
  messages = [];
  message = '';
  name = '';
  _socket = null;
  users = [];
  chatVisible = false;

  constructor(ChatService, $rootScope, ConfirmationModalService, $state) {
    'ngInject';
    if ($rootScope.username) {
      this._socket = ChatService;
      this._initSocket(ChatService, $rootScope, ConfirmationModalService, $state);
    }
  }

  $onDestroy() {
    this._socket.disconnect();
  }

  _initSocket(SocketService, $rootScope, ConfirmationModalService, $state) {
    this._socket.connect()

    this._socket.on('disconnect', (data) => {
      this._socket.disconnect();
      $rootScope.username = '';
    });

    this._socket.on('init:error', (data) => {
      let error = data.error;
      if (data.error == 'usertaken') {
        error = 'Username is already taken';
      }
      ConfirmationModalService.show(error, () => { });
    });

    this._socket.on('init', (data) => {
      this.name = data.name;
      this.users = data.users;
      $state.go('greeting');
    });

    this._socket.on('send:message', (message) => {
      this.messages.push(message);
    });

    this._socket.on('user:join', (data) => {
      this.messages.push({
        user: 'chatroom',
        text: 'User ' + data.name + ' has joined.'
      });
      this.users.push(data.name);
    });

    this._socket.on('user:left', (data) => {
      this.messages.push({
        user: 'chatroom',
        text: 'User ' + data.name + ' has left.'
      });
      for (let i = 0; i < this.users.length; i++) {
        let user = this.users[i];
        if (user === data.name) {
          this.users.splice(i, 1);
          break;
        }
      }
    });
  }

  sendMessage() {
    if (!this.message) return;

    this._socket.emit('send:message', {
      message: this.message
    });

    // add the message to our model locally
    this.messages.push({
      user: this.name,
      text: this.message
    });

    // clear message box
    this.message = '';
  };

  toggleChat() {
    this.chatVisible = !this.chatVisible;
  }
}

export default ChatController;
