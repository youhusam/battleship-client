import angular from 'angular';
import ChatComponent from './chat.component';
import uiRouter from 'angular-ui-router';
import ChatService from './chat.service'

let chatModule = angular.module('chat', [

])
.factory('ChatService', ChatService)
.component('chat', ChatComponent)
.name;

export default chatModule;
