import io from 'socket.io-client';

export default ($rootScope, $timeout) => {
  'ngInject'
  let socket;
  return {
    connect() {
      socket = io(`${process.env.SERVER}/chat`, { query: `username=${$rootScope.username}` });
    },
    disconnect() {
      socket.close();
    },
    on(eventName, callback) {
      socket.on(eventName, function () {
        let args = arguments;
        $timeout(() => {
          $rootScope.$apply(function () {
            callback.apply(socket, args);
          });
        });
      });
    },
    emit(eventName, data, callback) {
      socket.emit(eventName, data, function () {
        let args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    }
  };
}
