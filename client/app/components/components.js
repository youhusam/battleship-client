import angular from 'angular';
import Chat from './chat/chat';
import Login from './login/login';
import Game from './game/game';
import Greeting from './greeting/greeting';
import ConfirmationModal from './confirmationModal/confirmationModal';

let componentModule = angular.module('app.components', [
  Chat,
  Login,
  Game,
  Greeting,
  ConfirmationModal
])

.name;

export default componentModule;

