export default () => {
  'ngInject'

  let onShow = null;

  return {
    init(onShowFunction) {
      onShow = onShowFunction;
    },
    show(message, onYes, onNo) {
      onShow(message, onYes, onNo);
    }
  };
}
