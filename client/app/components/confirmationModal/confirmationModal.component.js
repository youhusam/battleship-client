import template from './confirmationModal.html';
import controller from './confirmationModal.controller';
import './confirmationModal.scss';

let confirmationModalComponent = {
  bindings: {},
  template,
  controller,
  controllerAs: 'confirmationModalController'
};

export default confirmationModalComponent;
