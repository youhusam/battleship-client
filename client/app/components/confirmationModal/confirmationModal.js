import angular from 'angular';
import confirmationModalComponent from './confirmationModal.component';
import confirmationModalService from './confirmationModal.service';

let confirmationModalModule = angular.module('confirmationModal', [

])

.factory('ConfirmationModalService', confirmationModalService)

.component('confirmationModal', confirmationModalComponent)

.name;

export default confirmationModalModule;
