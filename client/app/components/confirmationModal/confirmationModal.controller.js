class ConfirmationModalController {
  visible = false;

  constructor(ConfirmationModalService) {
    'ngInject';
    ConfirmationModalService.init(this.show.bind(this));
  }

  show(message, onYes, onNo) {
    this.visible = true;
    this.message = message;
    this.onYes = onYes;
    this.onNo = onNo;
  }

  hide() {
    this.visible = false;
    this.message = '';
    this.onYes = null;
    this.onNo = null;
  }

  handleYes() {
    if (this.onYes) {
      this.onYes();
    }
    this.hide();
  }

  handleNo() {
    this.onNo();
    this.hide();
  }
}

export default ConfirmationModalController;
