import angular from 'angular';
import uiRouter from 'angular-ui-router';
import greetingComponent from './greeting.component';

let greetingModule = angular.module('greeting', [
  uiRouter
])

  .config(($stateProvider) => {
    'ngInject';

    $stateProvider.state('greeting', {
      url: '/greeting',
      component: 'greeting'
    })
  })

.component('greeting', greetingComponent)

.name;

export default greetingModule;
