import template from './greeting.html';
import controller from './greeting.controller';
import './greeting.scss';

let greetingComponent = {
  bindings: {},
  template,
  controller,
  controllerAs: 'greetingController'
};

export default greetingComponent;
