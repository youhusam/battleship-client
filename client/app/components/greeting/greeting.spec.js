import GreetingModule from './greeting';
import GreetingController from './greeting.controller';
import GreetingComponent from './greeting.component';
import GreetingTemplate from './greeting.html';

describe('Greeting', () => {
  let $rootScope, makeController;

  beforeEach(window.module(GreetingModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new GreetingController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template [REMOVE]', () => {
      expect(GreetingTemplate).to.match(/{{\s?\$ctrl\.name\s?}}/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = GreetingComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(GreetingTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(GreetingController);
    });
  });
});
