import AuthController from '../../auth.controller';

class GreetingController extends AuthController {
  constructor($rootScope, $state, $timeout) {
    'ngInject';
    super($rootScope, $state);

    this.name = $rootScope.username;

    if (this.name) {
      $timeout(() => {
        $state.go('game');
      }, 1090);
    }
  }
}

export default GreetingController;
