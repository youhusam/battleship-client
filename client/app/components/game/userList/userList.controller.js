class UserListController {
  constructor(GameService) {
    'ngInject';
    this.users = GameService.getUsers();
  }

  challengeUser(user) {
    this.parent.challengeUser(user);
    this.opponentName = user;
  }
}

export default UserListController;
