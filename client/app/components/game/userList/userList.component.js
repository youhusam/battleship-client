import template from './userList.html';
import controller from './userList.controller';
import './userList.scss';

let userListComponent = {
  template,
  controller,
  controllerAs: 'userListController',
  require: {
    parent: '^^game'
  }
};

export default userListComponent;
