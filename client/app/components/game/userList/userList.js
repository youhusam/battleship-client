import angular from 'angular';
import uiRouter from 'angular-ui-router';
import userListComponent from './userList.component';

let userListModule = angular.module('game.userList', [
  uiRouter
])

.config(($stateProvider) => {
  'ngInject';

  $stateProvider.state('game.userList', {
    url: '/player-list', // Baaad
    component: 'userList'
  })
})

.component('userList', userListComponent)

.name;

export default userListModule;
