class BoardController {
  hits = {}
  ships = {}

  constructor() {
    'ngInject';
  }

  getNumberRange() {
    let numArray = new Array();
    for (let i = 0; i <= this.boardSize; i++) {
      numArray.push(i);
    }
    return numArray;
  }

  getLetterRange() {
    let letterArray = new Array();
    letterArray.push(0);
    for (let i = 0; i < this.boardSize; i++) {
      letterArray.push(String.fromCharCode('A'.charCodeAt() + i));
    }
    return letterArray;
  }

  handleBoardCellClick(row, col) {
    this.handleCellClick(row, col);
  }

  mouseDown(event, row, col) {
    if (event.which === 3 && this.handleRightClick) {
      event.preventDefault();
      this.handleRightClick(row, col);
    }
  }

  generateCellClassName(row, col) {
    const coords = `${row}${col}`;
    const classes = [
      this.hits[coords],
    ];

    if (this.ships[coords]) {
      classes.push('ship', this.ships[coords].orientation, this.ships[coords].pieceType);
    }

    if (this.tempShip && this.tempShip[coords])  {
      classes.push('temp-ship', this.tempShip[coords].orientation, this.tempShip[coords].pieceType);
    }

    return classes;
  }
}

export default BoardController;
