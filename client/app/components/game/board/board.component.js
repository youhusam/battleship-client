import template from './board.html';
import controller from './board.controller';
import './board.scss';

let boardComponent = {
  bindings: {
    ships: '<',
    hits: '<',
    handleCellClick: '<',
    boardSize: '<',
    tempShip: '<',
    handleHover: '<',
    handleRightClick: '<'
  },
  template,
  controller,
  controllerAs: 'boardController'
};

export default boardComponent;
