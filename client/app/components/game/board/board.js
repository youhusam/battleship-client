import angular from 'angular';
import boardComponent from './board.component';

let boardModule = angular.module('game.board', [
])

.component('board', boardComponent)

.name;

export default boardModule;
