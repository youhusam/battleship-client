import AuthController from '../../auth.controller';

class GameController extends AuthController {
  users = [];
  name = '';
  inGame = false;
  room = '';
  opponentHits = {};
  hits = {};
  ships = {};
  opponentShips = {};
  opponentName = '';
  _socket = null;
  waiting = false;
  hasTurn = false;
  canAttack = false;
  ready = false;
  gameReady = false;
  shipTypes = {
    ship1: 4,
    ship2: 2,
    ship3: 1,
    ship4: 1
  };
  boardSize = 10;

  constructor(GameService, $rootScope, ConfirmationModalService, $state) {
    'ngInject';
    super($rootScope, $state);

    if ($rootScope.username) {
      if (!GameService.isConnected()) {
        this._socket = GameService;
        this._initSocket(GameService, $rootScope, ConfirmationModalService, $state);
      }
    }
    this.challengeUser = this.challengeUser.bind(this);
    this.quit = this.quit.bind(this, $state);
    this.acceptChallenge = this.acceptChallenge.bind(this, $state);
    this.attack = this.attack.bind(this);
  }

  $onDestroy() {
    if (this.inGame || this.waiting) {
      this.quit();
    }
    this._socket && this._socket.disconnect();
  }

  _initSocket(SocketService, $rootScope, ConfirmationModalService, $state) {
    //region initialisation
    this._socket.connect();

    this._socket.on('disconnect', (data) => {
      this._socket.disconnect();
      $rootScope.username = '';
    });

    this._socket.on('init:error', (data) => {
      let error = data.error;
      if (data.error == 'usertaken') {
        error = 'Username is already taken';
      }
      ConfirmationModalService.show(error);
    });

    this._socket.on('init', (data) => {
      this.name = data.name;
      this.users = data.users;
      SocketService.setUsers(data.users);
    });
    //endregion

    //region users
    this._socket.on('user:join', (data) => {
      SocketService.addUser(data.name);
    });

    this._socket.on('user:left', (data) => {
      SocketService.removeUser(data.name);
      if (this.inGame && data.name === this.opponentName) {
        ConfirmationModalService.show(`${data.name} quit.`, () => {
          this.startGame();
          this.inGame = false;
        });
        $state.go('game.userList');
      }
    });
    //endregion

    //region pre game challenge handlers
    this._socket.on('challenge', (data) => {
      if (this.inGame) {
        this.declineChallenge('busy', data)
      } else {
        ConfirmationModalService.show(
          `${data.name} is challenging you! Accept?`,
          this.acceptChallenge.bind(this, data),
          () => this.declineChallenge('decline', data)
        );
      }
    });

    this._socket.on('challenge:accepted', (data) => {
      this.startGame();
      this.room = data.room;
      $state.go('game.inGame');
    });

    this._socket.on('challenge:declined', data => {
      this.startGame();
      this.inGame = false;
      const message = data.reason === 'busy' ? `${data.name} is busy` : `${data.name} has declined your challenge request`;
      ConfirmationModalService.show(message);
    });

    this._socket.on('challenge:quit', data => {
      this.startGame();
      this.inGame = false;
      $state.go('game.userList');
      ConfirmationModalService.show(`${this.opponentName} surrendered!`);
    });
    //endregion

    //region in game challenge handers

    this._socket.on('challenge:ready', (data) => {
      this.ready = true;
      this.canAttack = true;
      this.gameReady = true;
      this.hasTurn = data.turnFor === this.name;
    });

    this._socket.on('challenge:attackResult:miss', (data) => {
      if (data.user === this.name) {
        this.hasTurn = false;
        this.canAttack = false;
        this.hits[data.coords] = 'miss';
      } else {
        this.hasTurn = true;
        this.canAttack = true;
        this.opponentHits[data.coords] = 'miss';
      }
    });

    this._socket.on('challenge:attackResult:hit', (data) => {
      if (data.user === this.name) {
        this.hits[data.coords] = 'hit';
        this.canAttack = true;
      } else {
        this.opponentHits[data.coords] = 'hit';
      }
    });

    this._socket.on('challenge:attackResult:shipDestroyed', (data) => {
      if (data.user === this.name) {
        this.hits[data.coords] = 'hit';
        this.opponentShips = { ...this.opponentShips, ...data.ship }
        this.canAttack = true;
      } else {
        this.opponentHits[data.coords] = 'hit';
      }
    });

    this._socket.on('challange:attackResult:dup', data => {
      this.canAttack = true;
    });

    this._socket.on('challenge:attackResult:gameOver', (data) => {
      this.opponentShips = { ...this.opponentShips, ...data.ship }
      let message = 'You Lose!';
      if (data.user === this.name) {
        message = 'You Win!';
      }
      ConfirmationModalService.show(message, () => {
        this.startGame();
        this.inGame = false;
        $state.go('game');
      });
    });

    //endregion
  }

  startGame() {
    this.waiting = false;
    this.inGame = true;
    this.room = '';
    this.opponentHits = {}
    this.hits = {};
    this.ships = {};
    this.opponentShips = {};
    this.canAttack = false;
    this.ready = false;
    this.gameReady = false;
  }

  challengeUser(user) {
    this.opponentName = user;
    this.waiting = true;
    this._socket.emit('challenge', {
      user,
      name: this.name
    });
  }

  quit($state) {
    this._socket.emit('challenge:quit', {
      user: this.name,
      room: this.room
    });
    this.startGame();
    this.inGame = false;
    $state.go('game.userList');
  }

  declineChallenge(reason, data) {
    this._socket.emit('challenge:declined', {
      name: this.name,
      room: data.room,
      userId: data.from,
      reason
    });
  }

  acceptChallenge($state, data) {
    this.startGame();
    this.room = data.room;
    this.opponentName = data.name;
    this._socket.emit('challenge:accepted', {
      user: this.name,
      room: data.room,
      opponentName: data.name,
      userId: data.from
    });
    $state.go('game.inGame');
  }

  setReady() {
    this.placeShips();
    this.ready = true;
    this._socket.emit('challenge:userReady', {
      user: this.name,
      room: this.room
    });
  }

  attack(row, col) {
    console.log(row, col, this.canAttack);
    if (!this.canAttack || this.hits[`${row}${col}`]) return;

    this.canAttack = false;
    this._socket.emit('challenge:attack', {
      room: this.room,
      user: this.name,
      opponentName: this.opponentName,
      coords: `${row}${col}`
    });
  }

  placeShips() {
    this._socket.emit('challenge:placeShips', {
      user: this.name,
      room: this.room,
      ships: this.ships
    });
  }
}

export default GameController;
