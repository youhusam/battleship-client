import template from './inGame.html';
import controller from './inGame.controller';
import './inGame.scss';

let inGameComponent = {
  template,
  require: { parent: '^^game' },
  controller,
  controllerAs: 'inGameController'
};

export default inGameComponent;
