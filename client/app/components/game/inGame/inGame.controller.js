class InGameController {
  shipToInsert = '1';
  verticalShip = false;
  boardSize = 0;
  tempShip = {};

  constructor(ConfirmationModalService) {
    'ngInject';
    this.quit = this.quit.bind(this, ConfirmationModalService);
    this.placeShip = this.placeShip.bind(this);
    this.calculateShip = this.calculateShip.bind(this);
    this.positionShip = this.positionShip.bind(this);
    this.toggeleShipDirection = this.toggeleShipDirection.bind(this);
  }

  $onInit() {
    this.boardSize = this.parent.boardSize;
    this.largestRow = String.fromCharCode('A'.charCodeAt() + this.boardSize - 1);
  }

  quit(ConfirmationModalService) {
    ConfirmationModalService.show('Are you sure you want to surrender?', this.parent.quit, () => { });
  }

  calculateShip(row, col) {
    let shipName = `ship${this.shipToInsert}`
    if (this.parent.ready || this.parent.ships[`${row}${col}`] || !this.parent.shipTypes[shipName]) return;
    const shipSize = this.parent.shipTypes[shipName];

    let ship = {
      [`${row}${col}`]: {
        name: shipName,
        pieceType: shipSize > 1 ? 'front' : 'small',
        orientation: this.verticalShip ? 'vertical' : 'horizontal'
      }
    }

    for (let i = 1; i < shipSize; i++) {
      let coord;
      if (this.verticalShip) {
        const calculatedRow = String.fromCharCode(row.charCodeAt() + i);
        coord = `${calculatedRow}${col}`;
        if (this.parent.ships[coord] || calculatedRow > this.largestRow) return;
      } else {
        coord = `${row}${col + i}`;
        if (this.parent.ships[coord] || col + i > this.boardSize) return;
      }
      ship[coord] = {
        name: shipName,
        pieceType: i === shipSize - 1 ? 'back' : 'body',
        orientation: this.verticalShip ? 'vertical' : 'horizontal'
      }
    }

    return ship;
  }

  placeShip(row, col) {
    const ship = this.calculateShip(row, col);
    if (ship) {
      this.shipToInsert++;
      this.parent.ships = { ...this.parent.ships, ...ship };
      this.tempShip = {};
      if (this.shipToInsert > 4) {
        this.parent.setReady();
      }
    }
  }

  positionShip(row, col) {
    const ship = this.calculateShip(row, col);
    if (ship) {
      this.tempShip = ship;
    } else {
      this.tempShip = {};
    }
  }

  toggeleShipDirection(row, col) {
    this.verticalShip = !this.verticalShip;
    this.positionShip(row, col);
  }
}

export default InGameController;
