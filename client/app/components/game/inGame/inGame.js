import angular from 'angular';
import uiRouter from 'angular-ui-router';
import inGameComponent from './inGame.component';

let inGameModule = angular.module('game.inGame', [
  uiRouter
])

.config(($stateProvider) => {
  'ngInject';

  $stateProvider.state('game.inGame', {
    url: '/game',
    component: 'inGame'
  })
})


.component('inGame', inGameComponent)

.name;

export default inGameModule;
