import angular from 'angular';
import uiRouter from 'angular-ui-router';
import gameComponent from './game.component';
import MainMenu from './mainMenu/mainMenu';
import UserList from './userList/userList';
import Board from './board/board';
import InGame from './inGame/inGame';
import GameService from './game.service';

let gameModule = angular.module('game', [
  MainMenu,
  UserList,
  Board,
  uiRouter,
  InGame
])

.config(($stateProvider) => {
  'ngInject';

  $stateProvider.state('game', {
    url: '',
    component: 'game',
    redirectTo: 'game.mainMenu'
  });
})


.factory('GameService', GameService)
.component('game', gameComponent)

.name;

export default gameModule;
