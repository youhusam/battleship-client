import angular from 'angular';
import uiRouter from 'angular-ui-router';
import mainMenuComponent from './mainMenu.component';

let mainMenuModule = angular.module('game.mainMenu', [
  uiRouter
])

  .config(($stateProvider) => {
    'ngInject';

    $stateProvider.state('game.mainMenu', {
      url: '/main-menu', // Baaad
      component: 'mainMenu'
    })
  })


.component('mainMenu', mainMenuComponent)

.name;

export default mainMenuModule;
