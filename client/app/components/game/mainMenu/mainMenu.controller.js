class MainMenuController {
  inGame = false;

  constructor($state, $rootScope, GameService) {
    'ngInject';
    this.play = this.play.bind(this, $state.go)
    this.quit = this.quit.bind(this, GameService)
  }

  $onInit() {
    this.inGame = this.parent.inGame;
  }

  play(go) {
    if (this.inGame)
      go('game.inGame');
    else
      go('game.userList');
  }

  quit(GameService) {
    GameService.quit();
  }
}

export default MainMenuController;
