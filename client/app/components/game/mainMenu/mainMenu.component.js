import template from './mainMenu.html';
import controller from './mainMenu.controller';
import './mainMenu.scss';

let mainMenuComponent = {
  template,
  require: { parent: '^^game' },
  controller,
  controllerAs: 'mainMenuController'
};

export default mainMenuComponent;
