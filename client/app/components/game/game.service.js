import io from 'socket.io-client';

export default ($rootScope, $state, $timeout, ConfirmationModalService) => {
  'ngInject'
  let socket;
  let users = [];
  let isConnected = false;
  return {
    connect(namespace) {
      socket = io(`${process.env.SERVER}/game`, { query: `username=${$rootScope.username}` });
      isConnected = true;
    },
    disconnect() {
      $rootScope.username = '';
      socket.close();
      isConnected = false;
      $timeout(() => {
        $state.go('login');
      });
    },
    quit() {
      if ($rootScope.username) {
        ConfirmationModalService.show("Are you sure you want to quit?",
          () => {
            this.disconnect();
          },
          () => { });
      }
    },
    isConnected() {
      return isConnected;
    },
    getUsers() {
      return users;
    },
    addUser(user) {
      users.push(user);
    },
    removeUser(userToRemove) {
      for (let i = 0; i < users.length; i++) {
        if (users[i] === userToRemove) {
          users.splice(i, 1);
          break;
        }
      }
    },
    setUsers(newUsers) {
      users = newUsers;
      this.removeUser($rootScope.username);
    },
    on(eventName, callback) {
      socket.on(eventName, function () {
        let args = arguments;
        $timeout(() => {
          $rootScope.$apply(function () {
            callback.apply(socket, args);
          });
        });
      });
    },
    emit(eventName, data, callback) {
      socket.emit(eventName, data, function () {
        let args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    }
  };
}
