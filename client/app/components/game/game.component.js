import template from './game.html';
import controller from './game.controller';
import './game.scss';

let gameComponent = {
  bindings: {},
  template,
  controller,
  controllerAs: 'gameController'
};

export default gameComponent;
