import angular from 'angular';
import uiRouter from 'angular-ui-router';
import 'normalize.css';
import Common from './common/common';
import Components from './components/components';
import AppComponent from './app.component';
import ngAnimate from 'angular-animate';

angular.module('app', [
  uiRouter,
  Common,
  Components,
  ngAnimate
])
.config(($locationProvider, $stateProvider) => {
  "ngInject";
  // @see: https://github.com/angular-ui/ui-router/wiki/Frequently-Asked-Questions
  // #how-to-configure-your-server-to-work-with-html5mode
  $locationProvider.html5Mode(true).hashPrefix('!');
  $stateProvider.state('app', {
    url: '/',
    redirectTo: 'game'
  });
})

.component('app', AppComponent);
