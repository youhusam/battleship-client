class AuthController {
  constructor($rootScope, $state) {
    if (!$rootScope.username) {
      $state.go('login');
    }
  }
}

export default AuthController;
