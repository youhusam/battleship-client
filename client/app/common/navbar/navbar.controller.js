class NavbarController {
  constructor($rootScope, GameService) {
    'ngInject';
    this.name = $rootScope.username;
    this.logout = this.logout.bind(this, GameService);
  }

  logout(GameService, $event) {
    $event.preventDefault();
    GameService.quit();
  }
}

export default NavbarController;
