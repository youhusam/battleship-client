import angular from 'angular';
import menuLinkComponent from './menuLink.component';

let menuLinkModule = angular.module('menuLink', [

])

.component('menuLink', menuLinkComponent)

.name;

export default menuLinkModule;
