import template from './menuLink.html';
import './menuLink.scss';

let menuLinkComponent = {
  template,
  controllerAs: 'menuLinkController',
  transclude: true,
  bindings: {
    handler: '='
  }
};

export default menuLinkComponent;
