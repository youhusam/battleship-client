import angular from 'angular';
import Navbar from './navbar/navbar';
import Logo from './logo/logo';
import MenuLink from './menuLink/menuLink';

let commonModule = angular.module('app.common', [
  Navbar,
  Logo,
  MenuLink
])

.name;

export default commonModule;
