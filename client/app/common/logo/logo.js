import angular from 'angular';
import logoComponent from './logo.component';

let logoModule = angular.module('logo', [

])

  .component('logo', logoComponent)

  .name;

export default logoModule;
