import template from './<%= name %>.html';
import controller from './<%= name %>.controller';
import './<%= name %>.scss';

let <%= name %>Component = {
  bindings: {},
  template,
  controller,
  controllerAs: '<%= name %>Controller'
};

export default <%= name %>Component;
